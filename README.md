# Shared protos

Este repositorio contiene todos los protos que usarán tanto la API Gateway como los microservicios. 

## API Gateway
En el caso de la API Gateway es posible usar los protos como si fueran un paquete de npm utilizando un script que llame a este repositorio. Esto se logra añadiendo las siguientes líneas dentro de los scripts del archivo ```package.json```.

```
"proto:install": "npm i git+https://github.com/YOUR_USERNAME/grpc-nest-proto.git",
"proto:order": "protoc --plugin=node_modules/.bin/protoc-gen-ts_proto -I=./node_modules/grpc-nest-proto/proto --ts_proto_out=src/order/ node_modules/grpc-nest-proto/proto/order.proto --ts_proto_opt=nestJs=true --ts_proto_opt=fileSuffix=.pb",
"proto:product": "protoc --plugin=node_modules/.bin/protoc-gen-ts_proto -I=./node_modules/grpc-nest-proto/proto --ts_proto_out=src/product/ node_modules/grpc-nest-proto/proto/product.proto --ts_proto_opt=nestJs=true --ts_proto_opt=fileSuffix=.pb",
"proto:all": "npm run proto:auth && npm run proto:order && npm run proto:product"
```

Luego dentro de la API se pueden ejecutar estos comando:

```
$ npm run proto:install && npm run proto:all
```

* ```proto:install```: Instala los protos como un paquete de npm
* ```proto:all```: Genera los archivos ```pb.ts``` dentro de los módulos que se indiquen en el script.